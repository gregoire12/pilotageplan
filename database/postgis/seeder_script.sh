touch ~/.pgpass
chmod 600 ~/.pgpass
echo "*:*:$POSTGRES_DB:$POSTGRES_USER:$POSTGRES_PASSWORD" > ~/.pgpass
SOURCE_FILE="./data/source.osm.pbf"
if [ ! -f "$SOURCE_FILE" ] ; then
wget -O $SOURCE_FILE https://download.geofabrik.de/europe/france-latest.osm.pbf 
fi;
osm2pgsql --create --slim --cache 2000 --database $POSTGRES_DB --username $POSTGRES_USER --host $POSTGRES_HOST --port 5432 --style ./style.style $SOURCE_FILE
