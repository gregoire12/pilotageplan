pushd ./database/postgis
docker build -t osm2pgsql-importer .
docker run --name update_postgis -d -t --rm --network=pilotageplan_default -v $(pwd)/data:/app/data --env-file ../../.env --env POSTGRES_HOST=postgres -v $(pwd)/:/osm osm2pgsql-importer
popd