echo "This script queues the python task for osm ingestion in neo4j"
echo "It requires redis, the python workers, and the postgres server already up"
docker run --rm -it --network=host redis redis-cli -h localhost rpush ingest_queue ''
echo "Check your worker output to follow ingestion progress"