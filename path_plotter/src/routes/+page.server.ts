import {PathService} from "../services/path_service";


export const actions ={
    default: async ({cookies,request})=>{
        const data = await request.formData();
        const start_id = data.get("start_id");
        const end_id = data.get("end_id")
        PathService.launchTask(start_id,end_id)
        return {success:true}
    }
}