import {NavigatorService} from "./navigator_service";

export const PathService={
    launchTask(start_id,end_id){
        //For now we just call the navigator service, this is just a skeleton
        NavigatorService.find_best_path(start_id, end_id)
    }
}