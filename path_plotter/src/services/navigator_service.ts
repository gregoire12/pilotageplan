import Redis from "ioredis";


// Set up the Redis connection
const redis = new Redis({
  host: process.env.REDIS_HOST,  // Adjust according to your setup
  port: 6379
});


export const NavigatorService = {
  find_best_path(start_id, end_id){
    redis.rpush("path_queue",JSON.stringify([start_id,end_id]))
  }
}

