import pytest

from cartographer import ingest_osm_database
from navigator import find_best_path
from services.OSMService import OSMService


class TestNavigator:
    @pytest.fixture(autouse=True,scope="class")
    def graph_setup(self,clean_graph,class_database_loader):
        """
        To setup the graph we load our example data and ingest it using the classic algorithm
        This approache has the advantage of avoiding to have complicated graph database restoring mechanisme, as neo4j doesn't allow for postgres style COPY TO and COPY FROM
        However, this should not be done on to complicated dataset, as ingest_osm_database might take to much time and performances.
        """
        class_database_loader("airodromes_points.csv",OSMService.POINTS_TABLE)
        class_database_loader("airodromes_ways.csv", OSMService.WAYS_TABLE)
        ingest_osm_database()
        yield
        clean_graph()
    def test_simple_path_computation(self):
        with OSMService.get_instance() as osm_service:
            lfpz = osm_service.query_points_with_tag(icao="LFPZ")[0]
            lfpe = osm_service.query_ways_with_tag(icao="LFPE")[0]
        path = find_best_path(lfpz.id, lfpe.id)
        assert path.start.osm_id == lfpz.id, "the returned path should start at the start point LFPZ"
        assert path.end.osm_id == lfpe.id, "The returned path should end at the end point LFPE"
        assert len(path.steps)==2, "The path should be direct for now, and so just the start and end in the steps ( as the graph is complete ) "

