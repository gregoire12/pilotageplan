import pytest

from cartographer import ingest_osm_database
from services.GraphService import GraphService
from services.OSMService import OSMService


class TestCartographer():
    def test_basic_ingestion(self,database_loader):
        database_loader("airodromes_points.csv",OSMService.POINTS_TABLE)
        database_loader("airodromes_ways.csv",OSMService.WAYS_TABLE)
        ingest_osm_database()
        with GraphService.get_instance() as graph_service:
            nodes = graph_service.get_all_nodes()
            edges = graph_service.get_all_edges()
        assert len(nodes)==10,"We take 10 points amongst all the airodromes"
        assert len(edges)==90,"Graph should be complete"
    @pytest.fixture(scope="class",autouse=True)
    def setup_graph_database(self,clean_graph):
        #There is nothing to do before the test for now
        yield
        clean_graph()
