import pytest

from services.OSMService import OSMService


class TestOSMService:
    def test_get_points_by_tags_not_null(self,database_loader):
        database_loader("airodromes_points.csv",OSMService.POINTS_TABLE)
        with OSMService.get_instance() as osm_service:
            points = osm_service.query_points_with_tag(icao=OSMService.NOT_NULL)
        assert len(points)==1, "There should be one point with icao not null"
        assert points[0].icao=="LFPZ"
    def test_get_ways_by_tags_not_null(self,database_loader):
        database_loader("airodromes_ways.csv",OSMService.WAYS_TABLE)
        with OSMService.get_instance() as osm_service:
            ways = osm_service.query_ways_with_tag(icao=OSMService.NOT_NULL)
        assert len(ways)==25, "There should be 25 ways with icao not null"
        icaos = list([way.icao for way in ways])
        assert "LFPG" in icaos, "Should include the LFPG airport"
        assert "LFPI" in icaos, "Should include the LFPI heliport"
    def test_get_way_by_tag(self,database_loader):
        database_loader("airodromes_ways.csv",OSMService.WAYS_TABLE)
        with OSMService.get_instance() as osm_service:
            ways = osm_service.query_ways_with_tag(icao="LFPG")
        assert len(ways)==1, "There is only one element with icao LFPG"
        assert ways[0].tags["name"]=="Aéroport de Paris-Charles-de-Gaulle", "LFPG should be Aeroport de paris charles de gaulle"
    def test_get_point_by_tag(self,database_loader):
        database_loader("airodromes_points.csv",OSMService.POINTS_TABLE)
        with OSMService.get_instance() as osm_service:
            points = osm_service.query_points_with_tag(icao="LFPZ")
        assert len(points)==1, "There is only one LFPZ"
        assert points[0].tags["name"]=="Aérodrome de Saint-Cyr-l'École", "The returned point is Saint cyr l'école"
    def test_get_airodromes(self,database_loader):
        database_loader("airodromes_points.csv",OSMService.POINTS_TABLE)
        database_loader("airodromes_ways.csv",OSMService.WAYS_TABLE)
        with OSMService.get_instance() as osm_service:
            airodromes = osm_service.get_icao_airodromes()
        assert len(airodromes)==26,"There should be 27 airodromes in total"
        icaos = list(a.icao for a in airodromes)
        assert "LFPZ" in icaos," LFPZ should be in the airodromes"
        assert "LFPG" in icaos," LFPG should be in the airodromes"
