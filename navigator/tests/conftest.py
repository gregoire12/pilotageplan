import logging
import os
from contextlib import contextmanager

import pytest
import psycopg2
from dotenv import load_dotenv
from neo4j import GraphDatabase
from neo4j.exceptions import ServiceUnavailable
from psycopg2 import OperationalError


@pytest.fixture(scope="session", autouse=True)
def load_test_env():
    load_dotenv("./tests/.env.test")


def db_host():
    return os.getenv("POSTGRES_HOST")


def db_port():
    return 5432


def db_name():
    return os.getenv("POSTGRES_DB")


def db_user():
    return os.getenv("POSTGRES_USER")


def db_password():
    return os.getenv("POSTGRES_PASSWORD")


def is_postgres_responsive():
    try:
        psycopg2.connect(
            dbname=db_name(),
            user=db_user(),
            password=db_password(),
            host=db_host(),
            port=db_port()
        )
        return True
    except OperationalError:
        return False
def is_neo4j_responsive():
    try:
        GraphDatabase.driver(f"bolt://{os.getenv('GRAPH_HOST')}:7687", auth=(
            os.getenv("NEO4J_AUTH").split("/")[0], os.getenv("NEO4J_AUTH").split("/")[1])).verify_connectivity()
        return True
    except ServiceUnavailable:
        return False


@pytest.fixture(scope="class")
def class_database_loader(docker_services):
    with database_loader_impl(docker_services) as result:
        yield result


@pytest.fixture(scope="function")
def database_loader(docker_services):
    """ A fixture loading database file and cleaning up afterwards"""
    with database_loader_impl(docker_services) as result:
        yield result

@contextmanager
def database_loader_impl(docker_services):
    docker_services.wait_until_responsive(
        timeout=30.0, pause=1, check=lambda: is_postgres_responsive()
    )
    tables = []

    def loader(filename, table):
        logging.debug("Loading $s into $s", filename, table)
        with psycopg2.connect(
                dbname=db_name(),
                user=db_user(),
                password=db_password(),
                host=db_host(),
                port=db_port()
        ) as conn:
            with open("./tests/resources/" + filename, "r") as file:
                with conn.cursor() as cursor:
                    cursor.copy_from(file, table, sep=',', null="")
        tables.append(table)

    yield loader
    conn = psycopg2.connect(
        dbname=db_name(),
        user=db_user(),
        password=db_password(),
        host=db_host(),
        port=db_port()
    )
    logging.debug("Cleaning up tables: %s", ",".join(tables))
    with conn.cursor() as cursor:
        cursor.execute(f"""TRUNCATE {','.join(tables)};""")
    conn.commit()
    conn.close()


@pytest.fixture(scope="class")
def clean_graph(docker_services):
    docker_services.wait_until_responsive(
        timeout=30.0, pause=1, check=lambda: is_neo4j_responsive()
    )
    def cleaner():
        driver = GraphDatabase.driver(f"bolt://{os.getenv('GRAPH_HOST')}:7687", auth=(
            os.getenv("NEO4J_AUTH").split("/")[0], os.getenv("NEO4J_AUTH").split("/")[1]))
        driver.execute_query("match ()-[r]->() delete r")
        driver.execute_query("match(n) delete n")

    return cleaner
