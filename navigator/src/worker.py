import json
import logging
import os
from json import JSONDecodeError
from dotenv import load_dotenv
load_dotenv(dotenv_path="../.env")

from redis import Redis
from navigator import find_best_path
from cartographer import ingest_osm_database
from services import init_services

# We associate one handle to each queue
queues = {"path_queue":find_best_path,"ingest_queue":ingest_osm_database}
redis_conn = Redis(host=os.getenv("REDIS_HOST"), port=6379)
init_services()
logging.basicConfig(level=logging.DEBUG)
if __name__ == '__main__':
    while True:
        try:
            logging.info("Listening on task queues")
            queue, task = map(lambda s:s.decode("utf-8"),redis_conn.blpop(list(queues.keys())))
            args = []
            if len(task)>0 and task[0]=='[':
                args = json.loads(task)
            logging.debug("Received on %s the task %s ",queue, task)
            queues[queue](*args)
        except JSONDecodeError as e:
            logging.error("failed to json load")
            logging.error(queue+"\n"+task)
            logging.error(e)
        except KeyError as e:
            logging.error("Queue not found")
            logging.error(queue)
            logging.error(queues)
            logging.error(e)