from model.OSMElement import OSMElement


class Way(OSMElement):
    """
    A way is a combination of multiple nodes in osm,
    For now it is represented through it's centroid.
    """
    def __init__(self, id,latitude,longitude,icao, **tags) -> None:
        super().__init__(id, **tags)
        self.latitude = latitude
        self.longitude = longitude
        self.icao=icao