class OSMElement:
    """
    An dataelement from osm, has and id and a tag
    """
    def __init__(self,id,**tags) -> None:
        self.id = id
        self.tags=tags
