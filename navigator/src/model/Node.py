class Node:
    """
        Graph database node
    """

    POINT = "POINT"
    WAY = "WAY"
    REL = "REL"

    def __init__(self, osm_id: str, type: str, latitude: float, longitude: float) -> None:
        super().__init__()
        self.osm_id = osm_id
        self.type = type
        self.latitude = latitude
        self.longitude = longitude
