from model.OSMElement import OSMElement


class Point(OSMElement):
    """Punctual osm gis element, has a location """
    def __init__(self, id: str, longitude: float, latitude: float,icao=None,**tags) -> None:
        super().__init__(id,**tags)
        self.longitude = longitude
        self.latitude = latitude
        self.icao=icao
        self.tags = tags
