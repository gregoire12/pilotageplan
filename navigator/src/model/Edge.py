class Edge:
    """Graph database edge, always oriented"""
    def __init__(self,origin,destination,length) -> None:
        self.origin = origin
        self.destination = destination
        self.distance = length