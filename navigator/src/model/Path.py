from typing import List

from model.Node import Node


class Path:
    """A computed path from start to end"""

    def __init__(self,start : Node,end:Node,steps:List[Node]):
        super().__init__()
        self.start=start
        self.end=end
        self.steps = steps
