import time

from model.Path import Path
from services.GraphService import GraphService


def find_best_path(start_osm_id,end_osm_id) -> Path:
    with GraphService.get_instance() as graph_service:
        results=  graph_service.run_Astar(start_osm_id,end_osm_id)
        return Path(results[0],results[-1],results)