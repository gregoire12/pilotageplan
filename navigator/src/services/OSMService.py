import logging
import os
from time import sleep
from typing import List

import psycopg2
from psycopg2 import OperationalError

from model.OSMElement import OSMElement
from model.Point import Point
from model.Way import Way


def init_osm_service():
    OSMService.get_instance()


class OSMService:
    """
        Handle the osm database connection lifecyle, and queries it for the required information
        This is used to encapsulate the database access.
        API should be as business oriented as possible.

    """
    WAYS_TABLE = "planet_osm_polygon"
    POINTS_TABLE = "planet_osm_point"
    NOT_NULL = "__NOT_NULL__"
    __instance = None

    @classmethod
    def get_instance(cls) -> "OSMService":
        if cls.__instance is None:
            cls.__instance = OSMService()
        return cls.__instance

    def __init__(self):
        self.conn = None

    def __enter__(self):
        if self.conn is not None:
            raise RuntimeError("Can't re-enter in a osmservice, this is not supported for now")
        self.__connect()
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__disconnect()

    def __connect(self):
        tries = 3
        while tries >= 0:
            try:
                self.conn = psycopg2.connect(dbname=os.getenv("POSTGRES_DB"), user=os.getenv("POSTGRES_USER"),
                                             password=os.getenv("POSTGRES_PASSWORD"), host=os.getenv("POSTGRES_HOST"))
                break
            except OperationalError as e:
                if tries == 0:
                    raise e
                tries -= 1
                logging.warning("Failed Connecting to osm service, retrying in 5 seconds")
                sleep(5)
        self.create_temp_tables()

    def __disconnect(self):
        self.conn.commit()
        self.conn.close()
        self.conn = None

    def create_temp_tables(self):
        """To make individual queries faster we create temporary tables that contain the individual elements we are going to work on in the postgres database"""
        with self.conn.cursor() as cursor:
            query = """CREATE TEMP TABLE temp_point_of_interest AS
                        (SELECT * FROM planet_osm_point as way
                        WHERE  way.leisure = 'stadium' OR way.leisure = 'park' OR way.leisure = 'golf_course' OR way.aeroway = 'aerodrome' OR way.aeroway = 'airport' OR way.man_made = 'tower' OR way.bridge = 'yes' OR way.bridge = 'suspension' OR way.bridge = 'viaduct' OR way.natural = 'wood' OR way.natural = 'water' )"""
            cursor.execute(query)
        with self.conn.cursor() as cursor:
            query = """CREATE TEMP TABLE temp_area_of_interest AS
                        (SELECT * FROM planet_osm_polygon as way
                        WHERE  way.leisure = 'stadium' OR way.leisure = 'park' OR way.leisure = 'golf_course' OR way.aeroway IS NOT null OR way.man_made = 'tower' OR way.bridge = 'yes' OR way.bridge = 'suspension' OR way.bridge = 'viaduct' OR way.natural = 'wood' OR way.natural = 'water' )"""
            cursor.execute(query)

    def is_osm_update_running(self):
        query = """ SELECT * FROM pg_stat_activity WHERE application_name LIKE '%osm2pgsql%' AND state = 'active'; """
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            count = cursor.rowcount
        return count != 0

    def are_tables_ready(self):
        querry = """SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'planet_osm_point'"""
        with self.conn.cursor() as cursor:
            cursor.execute(querry)
            count = cursor.rowcount
        return count == 1

    def get_icao_airodromes(self) -> List[OSMElement]:
        points = self.query_points_with_tag(icao=self.NOT_NULL)
        ways = self.query_ways_with_tag(icao=self.NOT_NULL)
        return [*points, *ways]

    def query_points_with_tag(self, **tags) -> List[Point]:
        items = list(tags.items())
        keys, values = list(map(lambda x: x[0], items)), list(map(lambda x: x[1], items))
        query = f"""SELECT point.osm_id,
                          ST_X(ST_Transform(point.way,4326)) AS lon,
                          ST_Y(ST_Transform(point.way,4326)) AS lat,
                          point.name,*
                          FROM temp_point_of_interest as point 
                          WHERE {self.build_where_clause('point', items)} 
        """
        points = []
        with self.conn.cursor() as cursor:
            cursor.execute(query, values)
            data = cursor.fetchone()
            while data:
                id, longitude, latitude, name, *_ = data
                points.append(Point(id, longitude, latitude,
                                    **{cursor.description[index].name: data[index] for index in range(len(data))}))
                data = cursor.fetchone()
        return points

    def query_ways_with_tag(self, **tags) -> List[Way]:
        items = list(tags.items())
        values = list(map(lambda x: x[1], items))
        query = f"""SELECT way.osm_id,
                          ST_X(ST_CENTROID(ST_Transform(way.way,4326))) AS lon,
                          ST_Y(ST_CENTROID(ST_Transform(way.way,4326))) AS lat,
                          way.name,*
                          FROM temp_area_of_interest as way 
                          WHERE {self.build_where_clause('way', items)} 
        """
        ways = []
        with self.conn.cursor() as cursor:
            cursor.execute(query, values)
            data = cursor.fetchone()
            while data:
                id, longitude, latitude, name, *_ = data
                ways.append(Way(id, longitude, latitude,
                                **{cursor.description[index].name: data[index] for index in range(len(data))}))
                data = cursor.fetchone()
        return ways

    def build_where_clause(self, table_name, items):
        clause_elements = []
        for key, value in items:
            right = " = "
            if value == self.NOT_NULL:
                right = " IS NOT NULL"
            else:
                right += " %s "
            clause_elements.append(table_name + "." + key + right)
        return " and ".join(clause_elements)
