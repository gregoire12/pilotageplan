import logging
import os
from time import sleep
from typing import List

from neo4j import GraphDatabase
from neo4j.exceptions import ServiceUnavailable

from model.Edge import Edge
from model.Node import Node


def init_graph_service():
    GraphService.get_instance()


class GraphService():
    __instance = None

    @classmethod
    def get_instance(cls):
        if cls.__instance is None:
            cls.__instance = GraphService()
        return cls.__instance

    def __init__(self) -> None:
        self.driver = None

    def __enter__(self) -> "GraphService":
        if self.driver != None:
            raise RuntimeError(
                "driver already initialized, this service is already oppened, or hasen't bee properly exited")
        self.__connect__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__disconnect__()
        self.driver = None

    def __connect__(self):
        self.driver = GraphDatabase.driver(f"bolt://{os.getenv('GRAPH_HOST')}:7687", auth=(
            os.getenv("NEO4J_AUTH").split("/")[0], os.getenv("NEO4J_AUTH").split("/")[1]))
        tries = 3
        while tries >= 0:
            try:
                self.driver.verify_connectivity()
                break
            except ServiceUnavailable as e:
                if tries == 0:
                    raise e
                tries -= 1
                logging.warning("Failed connection to graph service, retrying in 5 seconds")
                sleep(5)

    def __disconnect__(self):
        self.driver.close()

    def create_node(self, node: Node) -> Node:
        summary = self.driver.execute_query(
            """MERGE (:WAYPOINT {type:$type,osm_id:$osm_id,latitude:$latitude, longitude:$longitude})""",
            type=node.type,
            osm_id=node.osm_id,
            latitude=node.latitude,
            longitude=node.longitude
        ).summary
        if summary.counters.nodes_created != 1:
            raise Exception("Failed to create node, " + summary.query)
        return node

    def create_edge(self, a: Node, b: Node, length: float):
        summary = self.driver.execute_query("""
            MATCH (a:WAYPOINT {type:$typeA, osm_id:$osm_id_a}) , (b:WAYPOINT {type:$typeB,osm_id:$osm_id_b})
            MERGE (a)-[:ROUTE {length:$length}]->(b)
            """,
                                            typeA=a.type,
                                            osm_id_a=a.osm_id,
                                            typeB=b.type,
                                            osm_id_b=b.osm_id,
                                            length=length
                                            ).summary
        logging.debug(summary.counters)

    def get_all_nodes(self) -> List[Node]:
        records, summary, keys = self.driver.execute_query("MATCH (n) RETURN n")
        nodes = []
        for record in records:
            data = record['n']
            nodes.append(self.parse_node(data))
        return nodes

    def parse_node(self, data):
        return Node(data["osm_id"], data["type"], data["latitude"], data["longitude"])

    def get_all_edges(self) -> List[Edge]:
        records, summary, keys = self.driver.execute_query("MATCH (start)-[edge]->(end) return start,edge,end")
        edges = []
        for r in records:
            edges.append(Edge(self.parse_node(r["start"]), self.parse_node(r["end"]), r["edge"]["length"]))
        return edges

    def run_Astar(self, start_osm_id, end_osm_id):
        result, _, __ = self.driver.execute_query("""
        MATCH (source:WAYPOINT {osm_id:$osm_id_source}), (target:WAYPOINT {osm_id:$osm_id_target})
            CALL gds.shortestPath.astar.stream('routing_graph', {
                sourceNode: source,
                targetNode: target,
                latitudeProperty: 'latitude',
                longitudeProperty: 'longitude',
                relationshipWeightProperty: 'length'
            })
            YIELD index, sourceNode, targetNode, totalCost, nodeIds, costs, path
            RETURN nodes(path) as path
            ORDER BY index
        """, osm_id_source=start_osm_id, osm_id_target=end_osm_id)
        return list(map(self.parse_node,result[0]["path"]))

    def create_graph(self,graph_name,node_projection,relation_projection,node_properties,relationship_properties):
        self.driver.execute_query("CALL gds.graph.drop($graph_name,false)",graph_name=graph_name)
        self.driver.execute_query("""
        CALL gds.graph.project(
                                $graph_name,
                                $node_projection,
                                $relation_projection,
                                {
                                    nodeProperties: $node_properties,
                                    relationshipProperties: $relationship_properties
                                })""",
                                  graph_name=graph_name,
                                  node_projection=node_projection,
                                  relation_projection=relation_projection,
                                  node_properties=node_properties,
                                  relationship_properties=relationship_properties


                                  )
