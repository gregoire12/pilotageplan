from services.GraphService import init_graph_service
from services.OSMService import init_osm_service


def init_services():
    init_graph_service()
    init_osm_service()