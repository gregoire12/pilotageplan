import logging
from time import sleep

from model.Node import Node
from model.Point import Point
from services.GraphService import GraphService
from services.OSMService import OSMService


def check_osm_update_over():
    osmService = OSMService.get_instance()
    with osmService:
        while not osmService.are_tables_ready():
            logging.info("Osm Tables not ready, waiting 10 seconds")
            sleep(10)
        while osmService.is_osm_update_running():
            logging.info("Osm update in progress, waiting 10 seconds")
            sleep(10)
    logging.info("No osm update in progress, carrying on")


def create_final_graph():
    with GraphService.get_instance() as graph_service:
        graph_service.create_graph("routing_graph","WAYPOINT","ROUTE",['latitude','longitude'],'length')


def ingest_osm_database():
    """
    Scans the osm datasource, and sync the graph database with the relevant data
    for now this takes 10 POI in the osm source, and create a complete graph in the graph database
    """
    logging.info("Ingesting Osm database")
    check_osm_update_over()

    points = get_points_of_interest()
    logging.debug("Found %s points of interest", points)

    nodes = []
    for point in points:
        nodes.append(add_node(point))
    for i in range(len(nodes)):
        for j in range(len(nodes)):
            if i != j:
                link(nodes[i], nodes[j])
    create_final_graph()

def get_points_of_interest():
    osm_service = OSMService.get_instance()
    with osm_service:
        return osm_service.get_icao_airodromes()[:10]


def add_node(point: Point) -> Node:
    with GraphService.get_instance() as graph_service:
        return graph_service.create_node(Node(point.id, "POINT", point.latitude, point.longitude))


def link(a: Node, b: Node):
    with GraphService.get_instance() as graph_service:
        graph_service.create_edge(a, b, 1)
