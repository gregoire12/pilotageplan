import os

import psycopg2
from dotenv import load_dotenv
load_dotenv("../.env")

if __name__ == '__main__':
    print("This is a small cli utilitary to dump a local database in an interactive way")
    print("The files this produces are meant to be used in this project's test environement")
    print("Be carefull with your selection request, this is not a public tool, and doesn't aim to be safe ")
    query = input("Input your selection query ? type 'SELECT * FROM table WHERE selection' ")
    filename = input("filename ( csv ) ")
    conn = psycopg2.connect(dbname=os.getenv("POSTGRES_DB"), user=os.getenv("POSTGRES_USER"),password=os.getenv("POSTGRES_PASSWORD"), host=os.getenv("POSTGRES_HOST"))
    with open(filename,"w") as output:
        with conn.cursor() as cursor:
            cursor.copy_expert(f"COPY ({query}) TO stdout WITH csv",output)
