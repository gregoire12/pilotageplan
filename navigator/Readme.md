# Navigator
_Python workers, responsible for finding the shortest path between two points_
## Running
_You need to have a local redis server running, as indicated in the parent readme_

Run the following commands
```bash
    source ./venv/bin/activate
    python worker.py
```
This will start the worker listening on the `path_queue` and `ingest_queue` redis list for now.

## Structure

The worker is pretty barebone but we don't need something more advanced

The worker.py listen to our two task queues, `path_queue` and `ingest_queue` and 
associate to each queue a simple function.

if the list element is a json list it is passed as arguments to the associated handler