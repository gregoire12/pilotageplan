# PilotagePlan
_An app to plan a Pilotage navigation [More details here](https://scrawny-music-041.notion.site/TravelPlaner-8dbc5e2e2beb48058f5ef82d81e9018f?pvs=4)_

## Goal

Build a travel planning tool, for aerodrome to aerodrome path finding

Build a clean, opensource, tool, that I can share as an example of my abilities

## Installation
_Here is how to run the project locally, to deploy it to a server see deployment_
### Requirements
- Docker and Docker-Compose installed
- `./.env` file with the following format:
```
POSTGRES_DB=postgre_db
POSTGRES_USER=postgre_user
POSTGRES_PASSWORD=postgres_password
POSTGRES_HOST=localhost
NEO4J_AUTH=neo4j/neo4j_password
GRAPH_HOST=localhost
REDIS_HOST=localhost
```
feel free to update the values
### Before First Run
Run 
```bash 
cd ./path_plotter
npm install
```
```bash
cd ./navigator
python3 -m venv ./venv
source ./venv/bin/activate
pip install -r requirements.txt
```
[Optional BUT strongly recommended]
You will not want the default deployment coverage, over the all of france in local run, this migth be to long to deploy, to avoid so either:

Place your own `source.osm.pbf` in `./database/postgis/data`

Or running the following will make the project seed with the Ile-de-france Region ( paris and neightbouring regions )
```bash
wget -O ./database/postgis/data/source.osm.pbf https://download.geofabrik.de/europe/france/ile-de-france-latest.osm.pbf
```

### Running

To run locally in developement: 
```bash 
docker-compose up -d postgres neo4j redis
```
```bash
cd ./path_plotter
npm run dev
```
In another terminal
```bash
cd ./navigator
source ./venv/bin/activate
python ./worker.py
```
### After first run

You will need to seed your databases

```bash
bash ./database/postgis/update_postgis.sh
bash ./database/neo4j/ingest_osm.sh
```
_this should take a few minutes_ 


## Deployment

### Requirements
- Ansible installed
- An ansible vault placed in `ansible/secrets.yml` with the following structure:
```
postgres_db: 'your_db_name'
postgres_user: 'your_db_user'
postgres_password: 'your_db_password'

neo4j_password: 'your_neo4j_password'
```
- an inventory.ini with the following structure:
```
[servers]
serverAdress ansible_ssh_user=sshuser ansible_ssh_private_key_file=privatekey

[all:vars]
project_remote_path=local path to project
project_local_path=path on the remote server
```

### Deployment
_this will deploy with default data coverage (France)

Using ansible run:
- `ansible-playbook -i inventory.ini ansible/main.yml -K --ask-vault-pass`

Or use the script in 
` ./scripts/deploy.sh`

Your database is then importing. And **not** ready.

For now there is no way to check if the import is finished, this is out of the scope of the MVP

There is no current way to customise your area of final deploy either through the one command deploy

However you can adapt the local deploy method to supply your own source file on the server before launching the command ( the file must already be on the server, we do not sync the local source file)

## Usage
_WIP_

## Structure

-/ansible: ansible deployment configuration

-/navigator: contains the worker code responsible for computing the best posible path between two points

-/path_plotter: contains the sveltekit project, including both it's backend and frontend

-/database: postgres configuration and scripts

-/graph-database: neo4j configuration and scripts

## Intent Notes

This project is meant as a pleasure and portfolio project. As such part is showing what I know, part is pleasure, part is learning.
I try to put it explicitly here

Ansible: Trying to get a basic handle on how ansible works, although deployment is not that complicated and could be handled with simple scripts. Ansible is a good tool to have under one's belt. 

Sveltkit: Learning svelte and the usage of servers like sveltekit, as they are a more and more common pattern in frontend developement

## Authors and acknowledgment
Gregoire Peltier

## License
Under GNU GPL 3 , no close sourcing possible however you can contact me if you want more work done.

